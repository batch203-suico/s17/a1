/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function printIdentificationMessage(){
		let fullName = prompt('Enter your Full Name: ');
		let getAge = prompt('Enter your Age: ');
		let getLocation = prompt('Enter your Location: ');

		console.log('Hello, ' + fullName);
		console.log('You are '+getAge+' years old');
		console.log('You live in '+getLocation);
	}

	printIdentificationMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFavorateBand(){
		let firstBand = prompt('Enter your First Band: ');
		let secondBand = prompt('Enter your Second Band: ');
		let thirdBand = prompt('Enter your Third Band: ');
		let fourthBand = prompt('Enter your Fourth Band: ');
		let fifthBand = prompt('Enter your Fifth Band: ');

		console.log('1. '+firstBand);
		console.log('2. '+secondBand);
		console.log('3. '+thirdBand);
		console.log('4. '+fourthBand);
		console.log('5. '+fifthBand);
	}

	printFavorateBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favorateMovies(){
		let firstMovie = prompt('Enter your First Movie: ');
		let secondMovie = prompt('Enter your Second Movie: ');
		let thirdMovie = prompt('Enter your Third Movie: ');
		let fourthMovie = prompt('Enter your Fourth Movie: ');
		let fifthMovie = prompt('Enter your Fifth Movie: ');

		console.log('1. '+firstMovie);
		console.log('Rotten Tomatoes Rating: 97%');
		console.log('2. '+secondMovie);
		console.log('Rotten Tomatoes Rating: 96%');
		console.log('3. '+thirdMovie);
		console.log('Rotten Tomatoes Rating: 91%');
		console.log('4. '+fourthMovie);
		console.log('Rotten Tomatoes Rating: 93%');
		console.log('5. '+fifthMovie);
		console.log('Rotten Tomatoes Rating: 96%');
	}

	favorateMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friendOne = prompt("Enter your first friend's name:"); 
	let friendTwo = prompt("Enter your second friend's name:"); 
	let friendThree = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friendOne); 
	console.log(friendTwo); 
	console.log(friendThree); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);